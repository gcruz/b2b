export default{
    colorRojo:{
        color: '#D80212',
    },
    /* APPFOOTER */
    iconFA: {
        color: '#fff',
        fontSize: 24,
    },
    iconActive: {
        color: '#fff',
    },
    /* LOGIN */
    screenLogin:{
        flex: 1,
        resizeMode: 'cover',
    },
    rightBtn: {
        display: 'flex',
    },
    bodyH: {
        display: 'flex',
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    imgSideBar: {
        backgroundColor: '#D80212',
        height: 160,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    formIniciar:{
        flex: 2,
        paddingLeft: 16,
        paddingRight: 16,
    },
    itemLogin:{
    },
    iniciarSesionBtn: {
        marginTop: 45,
        width: '50%',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    iniciarSesionBtnText:{
        textTransform: 'capitalize',
        fontSize: 18,
        fontWeight: 'bold',
    },
    espaceLogin:{
        flex: 1,
    },
    pieLoginGestionUser:{
        flex: 1,
    },
    gestionLogin: {
        flex: 1,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    gestionTextLogin:{
        padding: 10,
        textAlign: 'center',
        marginBottom: 16,
        fontSize: 18,
    },
    gestionEmailBtn:{
        borderColor: '#7e7e7d',
        justifyContent: 'center',
        alignItems: 'center',
    },
    gestionEmailTxt:{
        color: '#7e7e7d',
        fontWeight: 'bold',
        textTransform: 'lowercase',
        fontSize: 18,
    },
    msgGestionLogin:{
        alignItems: 'center',
        borderBottomWidth: 0,
        borderBottomColor: undefined,
    },
    mailGestion:{
        borderBottomWidth: 0,
        borderBottomColor: undefined,
    },
    msgGestionR:{
        borderBottomWidth: 0,
        borderBottomColor: undefined,
        borderColor: undefined,
    },
    gestionTxtMsg:{
        color: '#D80212',
        fontSize: 20,
    },
    /* PERFIL */
    perfilView:{
        paddingTop: 35,
        paddingLeft: 16,
        paddingRight: 16,
        paddingBottom: 0,
        alignItems: 'center',
        flex: 1,
    },
    text1Perfil:{
        fontWeight: 'bold',
        fontSize:24,
        color: '#7e7e7d',
        textAlign: 'center',
        marginBottom: 16,
    },
    perfilOptions:{
        flex: 6,
        alignItems: 'center',
        paddingLeft: 40,
        paddingRight: 40,
    },
    perfilCards: {
        width: '100%',
        backgroundColor: '#D80212',
        borderRadius: 19,
    },
    perfilCardsItems:{
        backgroundColor: 'transparent',
    },
    iconPerfilItem:{
        paddingBottom: 0,
    },
    txtPerfilItem:{
        paddingTop: 0,
    },
    perfilCardsItemsBody:{
        alignItems: 'center',
    },
    perfilCardsItemsText:{
        fontSize: 20,
        color: '#fff',
    },
    perfilIcon:{
        height: 95,
        width: 95,
        marginLeft: 18,
        marginRight: 18,
    },
    /* HOME */
    segmentsFilters:{
        paddingTop: 10,
        borderRadius: 10,
        overflow: 'hidden',
        width: '100%',
    },
    btnSegmen:{
    
    },
    formFilterHome:{
        padding: 16,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    searchBarH: {
        borderColor: '#7e7e7d',
        borderWidth: 1,
        borderRadius: 10,
        paddingLeft: 5,
        paddingRight: 5,
        flex: 6,
    },
    searchBarHIcon:{
        color: '#7e7e7d',
        fontSize: 20,
        marginRight: 5,
    },
    btnBoxFilter: {
        marginLeft: 10,
        flex: 1,
    },
    btnFilter:{
        borderRadius: 10,
        padding: 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    iconFilterH:{
        marginLeft: 0,
        marginRight: 0,
    },
    textBtnSegmen: {
        textTransform: 'capitalize',
        fontSize: 16,
            paddingLeft: 20,
            paddingRight: 20,
    },
    btnSegmenFirstHome:{
        borderTopLeftRadius: 5, 
        borderBottomLeftRadius: 5,
        borderLeftColor: '#D80212',
        borderTopColor: '#D80212',
        borderBottomColor: '#D80212',
    },
    btnSegmenLastHome:{
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
    },
    formFilterHomeView:{
        padding: 10,
    },
    formFilterHomeItem:{
    },
    formFilterHomeBtn:{
    },
    containerEmpresasList:{
        paddingLeft: 16,
        paddingRight: 16,
    },
    cartEmpresas:{
        borderRadius: 10,
        overflow: 'hidden',
    },
    cartItemNamesEmpresas:{
        paddingBottom: 5,
        paddingTop: 5,
    },
    textNamesEmpresas:{
        fontWeight: 'bold',
        fontSize: 16,
    },
    cartItemDatesEmpresas:{
        paddingBottom: 4,
        paddingTop: 4,
        justifyContent: 'space-between',
    },
    textDatesEmpresas:{},
    cartItemDatesEmpresasBoxSectorBadge:{
        flexDirection: 'row',
    },
    empresaTouch:{
        width: '100%',
    },
    /* Filtros */
    masFiltrosBtn:{
        justifyContent: 'center',
        alignItems: 'center',
    },
    masFiltrosBtnText:{
        color:'#D80212',
        fontWeight: 'bold',
        textTransform: 'capitalize',
        fontSize: 18,
    },
    listItem:{
        borderBottomWidth: 0,
        borderBottomColor: undefined,
        marginLeft: 0,
        paddingBottom: 4,
        paddingTop: 4,
    },
    textItem:{
        fontSize: 18,
    },
    itemTitle:{
        color: '#D80212',
        marginLeft: 0,
        fontWeight: 'bold',
        paddingBottom: 10,
    },
    filtrarBtn: {
        marginTop: 45,
        alignSelf: 'center',
        justifyContent: 'center',
    },
    filtrarBtnText:{
        textTransform: 'capitalize',
        fontSize: 18,
        fontWeight: 'bold',
    },
    /* Leyenda */
    modalLeyendaBox: {
        marginBottom: 35,
        marginTop: 10,
        marginLeft: 16,
        marginRight: 16,
        height: '100%',
        maxWidth: '100%',
        borderRadius: 10,
        backgroundColor: 'white',
        height: 'auto',
        flex: 1,
        justifyContent: 'center',
        padding: 25,
        paddingTop: 20,
        paddingBottom: 5, 
    },
    LeyendaTitel:{
        color: '#7e7e7d',
        fontSize: 24,
        fontWeight: 'bold',
    },
    leyendaText:{
        color: '#7e7e7d',
        fontSize: 16,
    },
    leyendaSector:{
        marginLeft: 0,
        alignItems: 'center',
        borderBottomWidth: 0,
        borderBottomColor: undefined,
        marginBottom: 0,
        marginTop: 0,
        paddingBottom: 0,
        paddingTop: 5,
        paddingLeft: 0,
    },
    circulLeyenda:{
        margin: 4,
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        width: 40,
        paddingBottom: 0,
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 5,
        borderRadius: 30,
    },
    textcirculLeyenda:{
        fontSize: 24,
        paddingBottom: 0,
        paddingLeft: 0,
        paddingRight: 0,
    },
    /* RESULTADOS */
    circulSector: {
        margin: 2,
        justifyContent: 'center',
        alignItems: 'center',
        height: 27,
        width: 27,
    },
    textcirculSector:{
        color: '#fff',
        fontSize: 12,
        paddingBottom: 0,
        paddingTop: 0,
    },
    /* FICHA */
    containerFicha:{
        flex: 1,
    },
    contentFicha:{
    },
    formFicha: {
        padding: 16,
    },
    listFicha: {
        paddingLeft: 0,
        borderBottomWidth: 1,
        borderBottomColor: '#7e7e7d',
    },
    listItemFicha:{
        borderBottomWidth: 0,
        borderBottomColor: undefined,
        marginLeft: 0,
    },
    listItemFichaContac:{
        paddingTop: 20,
        borderBottomWidth: 0,
    },
    itemBodyFicha:{
        
    },
    tittleFicha:{
        color: '#D80212',
        marginLeft: 0,
        fontWeight: 'bold',
        paddingBottom: 15,
    },
    dateFicha:{
        color: '#7e7e7d',
        fontSize: 16,
        fontWeight: 'bold',
        marginLeft: 0,
    },
    listModal:{
        borderBottomWidth: 0,
    },
    listItemModal:{
        paddingBottom: 15,
        paddingTop: 0,
    },
    modalFicha: {
        margin: 0,
        backgroundColor: 'white',
        height: 'auto',
        flex: 0,
        bottom: 0,
        position: 'absolute',
        width: '100%',
        padding: 25,
        paddingBottom: 5,
    },
    btnPicker:{
        borderColor: '#7e7e7d',
        height: 35,
    },
    btnListFicha: {
        textTransform: 'capitalize',
        paddingLeft: 10,
        paddingRight: 10,
    },
    dateBtnListFichaSectores:{
        
    },
    dateBtnListFichaProducts:{
        
    },
    boxBtnLocationFicha:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 16,
    },
    btnAddLocationFicha:{
        height: 35,

    },
    textBtnAddLocationFicha:{
        textTransform: 'capitalize',
        color: '#D80212',
        paddingLeft: 10,
        paddingRight: 10,
    },
    btnVerLocationFicha:{
        height: 35,
        
    },
    textBtnVerLocationFicha:{
        textTransform: 'capitalize',
        color: '#fff',
        paddingLeft: 10,
        paddingRight: 10,
    },
    /* CONTACTOS */
    content:{
        padding: 16,
    },
    contentContactos:{
        backgroundColor: '#EBEBEB',
    },
    empresarialContacto: {
        borderRadius: 8,
        backgroundColor: '#fff',
        marginBottom: 10,
    },
    iconEmpresarialContacto:{
        color: '#D80212'
    },
    textEmpresarialContacto:{
        color: '#7e7e7d',
        fontSize: 18,
        fontWeight: 'bold',
    },
    contactoCards:{
        borderRadius: 8,
        overflow: 'hidden',
    },
    contactoCardsItems:{
    },
    contactoCardsBody:{
        flex: 1,
        width: '100%'
    },
    contactoCardTop:{
        borderBottomColor: '#7e7e7d',
        borderBottomWidth: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        paddingBottom: 5,
    },
    contactoCardBot:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        paddingTop: 5,
    },
    iconContact:{
        fontSize: 16,
        marginRight: 5,
    },
    contactDato:{
        flexDirection: 'row',
        alignItems: 'center',
    },
    contactText:{
        color: '#7e7e7d',
    },
    contactNombre:{

    },
    contactNombreText:{
        fontWeight: 'bold',
        fontSize: 16,
    },
    contactCargoScroll:{
        maxWidth: '45%',
    },
    contactCargo:{
    },
    contactllamar:{
    },
    contactMail:{

    },
    contactllamarText:{
        color: '#D80212',
        fontSize: 16,
    },
    contactMailText:{
        color: '#D80212',
        fontSize: 16,
    },
    tipoDocumentBtnText:{
        color: '#7e7e7d',
        fontSize: 16,
        textTransform: 'capitalize',
        marginLeft: 0,
    },
    boxBtnNewContact:{
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 50,
    },
    /* nuevo contacto */
    textBtnNewContact:{
        fontWeight: 'bold',
    },
    itemNewContact:{
        marginLeft: 0,
    },
    /* SIDEBAR */
    userBox:{
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        marginLeft: 5,
    },
    inicialUserBox:{
        borderRadius: 50,
        borderColor: '#fff',
        borderWidth: 5,
        height: 45,
        width: 45,
        justifyContent: 'center',
        alignItems: 'center',
    },
    inicialUserTxt:{
        color: '#fff',
        fontSize: 35,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    userNameBox:{

    },
    userName:{
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold',
        paddingBottom: 0,
        marginBottom: 0,

    },
    userCargo:{
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',

    },
    sideListPerfil:{
        marginTop: 50,
    },
    sideListSoport:{

    },
    sidebarTxtItem:{
        fontSize: 22,
        color: '#D80212',
    },
    itemNoBorde:{
        borderBottomWidth: 0,
        borderBottomColor: undefined,
    },
    itemBodyNoBorde:{
        borderBottomWidth: 0,
        borderBottomColor: undefined,
    },
    txtSideBCerrarS:{
        marginTop: 5,
        fontWeight: 'bold',
    },
    /* HISTORICO */

}